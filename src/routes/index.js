const User = require('./User');
const Category = require('./Category');
const Item = require('./Item');
const Order = require('./Order');

module.exports = {
    User, 
    Category,
    Item,
    Order
}