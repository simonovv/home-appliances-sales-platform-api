const sequelize = require('../helpers/database');
const Sequelize = require('sequelize');

const Category = require('../models/Category');

const Item = sequelize.define('Item', {
    id: {
        type: Sequelize.INTEGER,
        unique: true,
        autoIncrement: true,
        primaryKey: true
    },
    categoryId: {
        type: Sequelize.INTEGER,
        references: {
            key: 'id',
            model: Category
        },
        onDelete: 'SET NULL'
    },
    name: {
        type: Sequelize.STRING,
        unique: {
            msg: 'Item with this name already exists'
        },
        validate: {
            max(value) {
                if(value.length >= 25) return Promise.reject(new Error('Item name need to be less that 25'));
            },
            min(value) {
                if(value.length <= 3) return Promise.reject(new Error('Item name need to be more than 3'))
            }
        }
    },
    description: {
        type: Sequelize.STRING,
        validate: {
            min(value) {
                if(value.length <= 10) return Promise.reject(new Error('Description need to be more than 10'));
            }
        }
    },
    price: {
        type: Sequelize.INTEGER,
        validate: {
            min(value) {
                if(value <= 0) return Promise.reject(new Error('Price need to be more than 0'));
            },
            max(value) {
                if(value >= 9999) return Promise.reject(new Error('Price need to be less than 9999'));
            }
        }
    },
    img: {
        type: Sequelize.STRING,
        defaultValue: 'https://content2.onliner.by/catalog/device/header/562116e1e9e3d8c2553b13059ca4f758.jpeg'
    }
})

module.exports = Item;