const test = {
    DIALECT: 'mysql',
    DB_NAME: 'db',
    DB_USER: 'root',
    DB_PASSWORD: 'password'
}

const development = {
    DIALECT: 'mysql',
    DB_NAME: 'db',
    DB_USER: 'root',
    DB_PASSWORD: 'password'
}

module.exports =  {
    development,
    test
};
