const Sequelize = require('sequelize');
const sequelize =  require('../helpers/database');

module.exports = User = sequelize.define('User', {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        unique: true,
        primaryKey: true
    },
    email: {
        type: Sequelize.STRING,
        unique: {
            msg: 'This email alreay exists'
        },
        allowNull: false,
        validate: {
            isEmail: {
                msg: 'Incorect e-mail format'
            }
        }
    },
    password: {
        type: Sequelize.STRING,
        allowNull: false
    },
    role: {
        type: Sequelize.INTEGER,
        defaultValue: 0
    },
    token: {
        type: Sequelize.STRING
    }
})