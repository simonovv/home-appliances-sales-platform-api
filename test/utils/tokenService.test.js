const tokenService = require('../../src/utils/jwtoken');

describe('TokenServiceTest', () => {
  it('verifyToken_validToken_true', async (done) => {
    const token = 'eyJhbGciOiJIUzI1NiJ9.ZW1haWxAZ21haWwuY29t.bMEPw_CcFq1EqlNK-lSXh7nSrnvabYs9pfPKBcmN2ZY';
    const email = 'email@gmail.com';
    const data = await tokenService.verifyToken(token);
    expect(data).toEqual(email)
    done()
  })

  it('verifyToken_inValidToken_false', async (done) => {
    const token = 'eyJhbGcNiJ9.ZW1haWxAZ21haWwuY29t.bMEPw_CcFq1EqlNK-lSXh7nSrnvabYs9pfPKBcmN2ZY';
    const data = await tokenService.verifyToken(token);
    expect(data).toBe(false);
    done()
  })
})