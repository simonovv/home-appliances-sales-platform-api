const winston = require('winston');

const logger = winston.createLogger({
    level: 'info',
    format: winston.format.simple(),
    defaultMeta: {service: 'user-service'},
    transports: [
        new winston.transports.File({filename: 'combined.log'})
    ]
});

module.exports = logger;