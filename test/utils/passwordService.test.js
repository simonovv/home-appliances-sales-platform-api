const sinon = require('sinon');
const passwordService = require('../../src/utils/password');

describe('PasswordServiceTest', () => {
  it('comparePassword_validPassword_true', async (done) => {
    const password = 'password';
    const hash = '$2b$10$kAY4Vh9Dym5XIeBfaFSln.8UKherJkIXpQaldTrkJyBKl35QzYeHK'
    const data = await passwordService.comparePassword(password, hash);
    expect(data).toBe(true);
    done()
  })

  it('comparePassword_invalidPassword_false', async (done) => {
    const password = 'pass';
    const hash = '$2b$10$kAY4Vh9Dym5XIeBfaFSln.8UKherJkIXpQaldTrkJyBKl35QzYeHK';
    const data = await passwordService.comparePassword(password, hash);
    expect(data).toBe(false);
    done()
  })
})