const password = require('./password');
const token = require('./jwtoken');

module.exports = {
    password,
    token
}