const Item = require('../../../models/Item');
const Category = require('../../../models/Category');

const gettAllItems = async (req) => {
  const { query: { sort } } = req;
  let items;
  if(sort === 'DESC' || sort === 'ASC') {
    items = await Item.findAll({order: [['price', sort]]})
      .catch(err => Promise.reject(new Error(err.message)));
  }
  else {
    items = await Item.findAll()
      .catch(err => Promise.reject(new Error(err.message)));
  }

  return items;
};

const newItem = async (req) => {
  const { category, name, description, price, img } = req.body;
  const item = await Item.create({
    categoryId: category,
    name, 
    description,
    price,
    img
  }).catch(err => Promise.reject(new Error(err.message)));

  return item.dataValues;
};

const changeItem = async (req) => {
  const { id, category, description, name, price, img } = req.body;
  const item = await Item.update({
    categoryId: category,
    name,
    description,
    price,
    img
  }, 
  {where: {id: id}})
    .catch(err => Promise.reject(new Error(err.message)));

  return item;
};

const findItemByCategory = async (req) => {
  const { params: { categoryId } } = req;
  const items = await Item.findAll({where: {categoryId: categoryId}})
    .catch(err => Promise.reject(new Error(err.message)));
  
  return items;
};

const findItemById = async (req) => {
  const { params: { id }} = req;
  const item = await Item.findAll({where: {id: id}})
    .catch(err => Promise.reject(new Error(err.message)));

  return item[0];
};

const deleteItem = async (req) => {
  const { params: { id }} = req;
  await Item.destroy({where: {id: id}})
    .catch(err => Promise.reject(new Error(err.message)));

  return {};
}

module.exports = {
  gettAllItems,
  newItem,
  changeItem,
  findItemByCategory,
  findItemById,
  deleteItem
}