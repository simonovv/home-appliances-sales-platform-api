const authService = require('./services/User/auth-service');
const User = require('../models/User');

module.exports = {
    registerUser: async (req, res) => {
        try {
            const user = await authService.reg(req);
            res.status(200).send({user: user});
        }
        catch(err) {
            res.status(500).send({error: err.message});
        }
    },

    loginUser: async (req, res) => {
        try {
            const user = await authService.login(req);
            res.status(200).send({user: user});
        }
        catch(err) {
            res.status(500).send({error: err.message});
        }
    },

    updateUser: (req, res) => {
        User.update({
            id: req.params.id,
            email: req.body.email,
            password: req.body.password,
            token: req.body.token,
            role: req.body.role
        })
            .then(user => {
                res.status(200).send({user: user})
            })
            .catch(err => {
                res.status(500).send({error: err.errors})
            })
    },

    getAllUsers: (req, res) => {
        User.findAll()
            .then(users => {
                res.status(200).send({users: users})
            })
            .catch(err => {
                res.status(500).send({error: err.errors})
            })
    }
}