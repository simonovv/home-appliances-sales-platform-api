const sequelize = require('../helpers/database');
const Sequelize = require('sequelize');

const Order = sequelize.define('order', {
  id: {
    type: Sequelize.INTEGER,
    unique: true,
    autoIncrement: true,
    primaryKey: true
  },
  name: {
    type: Sequelize.STRING,
    allowNull: false,
    validate: {
      max(value) {
        if(value.length >= 25) return Promise.reject(new Error('Name need to be less that 25'));
      },
      min(value) {
          if(value.length <= 3) return Promise.reject(new Error('Name need to be more than 3'))
      }
    }
  },
  surname: {
    type: Sequelize.STRING,
    allowNull: false,
    validate: {
      max(value) {
        if(value.length >= 25) return Promise.reject(new Error('Surname need to be less that 25'));
      },
      min(value) {
        if(value.length <= 3) return Promise.reject(new Error('Surname need to be more than 3'))
      }
    }
  },
  email: {
    type: Sequelize.STRING,
    allowNull: false,
    validate: {
      isEmail: true
    },
  },
  phoneNumber: {
    type: Sequelize.STRING,
    allowNull: false
  },
  details: {
    type: Sequelize.STRING,
    allowNull: true,
    validate: {
      max(value) {
        if(value.length >= 1000) return Promise.reject(new Error('Details neet to be less than 1000'))
      }
    }
  },
  items: {
    type: Sequelize.JSON
  }
})

module.exports = Order;