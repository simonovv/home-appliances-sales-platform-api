FROM node:10-alpine

WORKDIR /app

COPY package.json ./

RUN npm install
RUN npm i -g nodemon

COPY . .

CMD ["node", "index.js"]
