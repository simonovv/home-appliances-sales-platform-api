const jsonwebtoken = require('jsonwebtoken');
const CONST = require('../constants/server');

const getToken = async (email) => {
  const token = await jsonwebtoken.sign(email, CONST.SECRET);
  return token;
};

const verifyToken = (token) => {
  let data;
  try {
    data = jsonwebtoken.verify(token, CONST.SECRET); 
  }
  catch(err) {
    return false;
  }
  return data;
};

module.exports = {
    getToken,
    verifyToken
}
