const bcrypt = require('bcryptjs');
const conf = require('../constants/server');

const hashPassword = async (password) => {
  const salt = await bcrypt.genSalt(conf.SALT_I);
  const hash = await bcrypt.hash(password, salt);
  return hash;
};

const comparePassword = async (password, existPassword) => {
  const isMatch = await bcrypt.compare(password, existPassword);
  return isMatch;
};

module.exports = {
    hashPassword,
    comparePassword
}
