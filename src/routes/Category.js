const categoryService = require('./services/Category/category-service');

module.exports = {
    getAllCategories: async (req, res) => {
        try {
            const categories = await categoryService.getAllCategoryies();
            res.status(200).send({categories: categories}); 
        }
        catch(err) {
            console.log("mgfjgklfd");
            res.status(500).send({error: err.message});
        }
    },


    newCategory: async (req, res) => {
        try {
            const category = await categoryService.createCategory(req);
            res.status(200).send({category: category});
        }
        catch(err) {
            res.status(500).send({error: err.message});
        }
    },

    updateCategory: async (req, res) => {
        try {
            const category = await categoryService.updateCategory(req);
            res.status(200).send({category: category});
        }
        catch(err) {
            res.status(500).send({error: err.message})
        }
    },

    deleteCategory: async (req, res) => {
        try {
            await categoryService.deleteCategory(req);
            console.log('ALL NORMAL');
            res.status(200).send();
        }
        catch(err) {
            console.log('ERROR');
            res.status(500).send({error: err.message})
        }
    }
}
