const Order = require('../../../models/Order');

const getAllOrders = async () => {
  const orders = await Order.findAll({raw: true})
    .catch(err => Promise.reject(new Error(err.message)));

  return orders;
};

const newOrder = async (req) => {
  const { name, surname, email, phone, items, details } = req.body;
  const order = await Order.create({
    name, 
    surname,
    email,
    phoneNumber: phone,
    details,
    items
  }, {raw: true}).catch(err => Promise.reject(new Error(err.message)));

  return order;
};

module.exports = {
  getAllOrders,
  newOrder
}