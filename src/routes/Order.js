const orderService = require('./services/Order/order-service');

module.exports = {
  getAllOrders: async (req, res) => {
    try {
      const orders = await orderService.getAllOrders();
      res.status(200).send({orders: orders});
    }
    catch(err) {
      res.status(500).send({error: err.message});
    }
  },

  newOrder: async (req, res) => {
    try {
      const order = await orderService.newOrder(req);
      res.status(200).send({order: order});
    }
    catch(err) {
      res.status(500).send({error: err.message});
    }
  }
}