const itemService = require('./services/Item/item-service');

module.exports = {
    getAllItems: async (req, res) => {
        try {
            const items = await itemService.gettAllItems(req);
            res.status(200).send({items: items});
        }
        catch(err) {
            res.status(500).send({error: err.message});
        }
    },

    newItem: async (req, res) => {
        try {
            const item = await itemService.newItem(req);
            res.status(200).send({item: item});
        }
        catch(err) {
            res.status(500).send({error: err.message});
        }
    },

    changeItem: async (req, res) => {
        try {
            const item = await itemService.changeItem(req);
            res.status(200).send({item: item});
        }
        catch(err) {
            res.status(500).send({error: err.message});
        }
    },

    findItemByCategory: async (req, res) => {
        try {
            const items = await itemService.findItemByCategory(req);
            res.status(200).send({items: items});
        }
        catch(err) {
            res.status(500).send({error: err.message});
        }
    },

    findItemById: async (req, res) => {
        try {
            const item = await itemService.findItemById(req);
            res.status(200).send({item: item});
        }
        catch(err) {
            res.status(500).send({error: err.message});
        }
    },

    deleteItem: async (req, res) => {
        try {
            await itemService.deleteItem(req);
            res.status(200).send({});
        }
        catch(err) {
            res.status(500).send(err.message);
        }
    }
}