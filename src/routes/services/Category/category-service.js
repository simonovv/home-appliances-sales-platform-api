const Category = require('../../../models/Category');

const getAllCategoryies = async () => {
  const categories = await Category.findAll();

  if(!categories) {
    return new Promise(new Error('Error occured while processing categories'));
  }

  return categories;
};

const createCategory = async (req) => {
  const { name, img } = req.body;
  const category = await Category.create({
    name,
    img
  }).catch(err => Promise.reject(new Error(err.message)));

  return category.dataValues;
};

const updateCategory = async (req) => {
  const { name, img } = req.body;
  const { id } = req.params;
  const category = await Category.update({
    name,
    img
  },
  {where: {id: id}}).catch(err => Promise.reject(new Error(err.message)));

  return category;
};

const deleteCategory = async (req) => {
  const { id } = req.params;
  Category.destroy({where: {id: id}}).catch(err => Promise.reject(new Error(err.message)));
  return null;
};

module.exports = {
  getAllCategoryies,
  createCategory,
  updateCategory,
  deleteCategory
};