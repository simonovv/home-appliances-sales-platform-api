const express = require('express');
const app = express();
const router = express.Router();
const logger = require('./src/helpers/logger');

const bodyParser = require('body-parser');

const CONST = require('./src/constants/server');

const Routes = require('./src/routes/index');

app.use(bodyParser.json());

app.use(function (req, res, next) {
    logger.info(`Request: ${JSON.stringify(req, censor(req))}`);
    logger.info(`Response: ${JSON.stringify(res, censor(res))}`);
    next();
});

function censor(censor) {
    var i = 0;

    return function(key, value) {
        if(i !== 0 && typeof(censor) === 'object' && typeof(value) == 'object' && censor == value)
            return '[Circular]';

        if(i >= 29) // seems to be a harded maximum of 30 serialized objects?
            return '[Unknown]';

        ++i; // so we know we aren't using the original object anymore

        return value;
    }
}

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    next();
  });

//SUNC DB TABLES

const Models = require('./src/models/index');

// Models.User.sync({force: true})
// Models.Category.sync({force: true})
// Models.Item.sync({force: true})

//SUNC ALL DB TABLES

const sequelize = require('./src/helpers/database');
sequelize.sync();
// sequelize.sync({force: true});

//USER

app.post('/api/register', Routes.User.registerUser);

app.post('/api/login', Routes.User.loginUser);

app.get('/api/users', Routes.User.getAllUsers);

app.put('/api/user', Routes.User.updateUser);

//CATEGORY

app.get('/api/category', Routes.Category.getAllCategories);

app.post('/api/category', Routes.Category.newCategory);

app.post('/api/category/:id', Routes.Category.updateCategory);

app.post('/api/category/delete/:id', Routes.Category.deleteCategory);

//ITEMS

app.get('/api/items', Routes.Item.getAllItems);

app.post('/api/item', Routes.Item.newItem);

app.get('/api/item/:id', Routes.Item.findItemById);

app.get('/api/items/:categoryId', Routes.Item.findItemByCategory);

app.post('/api/updateItem', Routes.Item.changeItem)

app.post('/api/item/:id', Routes.Item.deleteItem)

//ORDER

app.post('/api/order', Routes.Order.newOrder);

app.get('/api/orders', Routes.Order.getAllOrders);

const port = process.env.PORT || CONST.PORT;

if(process.env.NODE_ENV !== 'test') {
    app.listen(port, ( ) => {
        console.log(`Server running on ${port}`);
    });
}

module.exports = app;