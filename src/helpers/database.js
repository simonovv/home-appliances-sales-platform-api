const Sequelize = require('sequelize');
const logger = require('./logger');

const CONST = require('../constants/database')[process.env.NODE_ENV === 'test' ? 'test' : 'development'];

const sequelize = new Sequelize(CONST.DB_NAME, CONST.DB_USER, CONST.DB_PASSWORD, {
    host: 'mysql',
    dialect: CONST.DIALECT,
    define: {
        timestamps: false,
    },
    port: 3306,
    logging: logger.info
});

module.exports = sequelize;
