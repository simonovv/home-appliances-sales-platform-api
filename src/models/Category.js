const sequelize = require('../helpers/database');
const Sequelize = require('sequelize');

const Category = sequelize.define('Category', {
    id: {
        type: Sequelize.INTEGER,
        unique: true,
        primaryKey: true, 
        autoIncrement: true,
        onDelete: 'SET NULL',
    },
    name: {
        type: Sequelize.STRING,
        unique: {
            msg: 'Item with this name already exists'
        },
        validate: {
            min(value) {
                if(value.length <= 3) return Promise.reject(new Error('Name need to be more than 3'))
            },
            max(value) {
                if(value.length >= 25) return Promise.reject(new Error('Name need to be less than 25'))
            }
        }
    },
    img: {
        type: Sequelize.STRING,
        defaultValue: 'https://cs6.pikabu.ru/post_img/big/2017/06/22/4/1498108182155540748.jpg'
    }
})

module.exports = Category;