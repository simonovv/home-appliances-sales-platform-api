const User = require('../../../models/User');
const Utils = require('../../../utils/index');

const reg = async (req) => {
  const { email, password } = req.body;
  const user = await User.findOne({
    where: { email: email },
  });

  if(!user) {
    let createdUser;
    createdUser = await User.create({
      email,
      password: await Utils.password.hashPassword(password)
    }).catch(err => Promise.reject(new Error(err.message)));
    return createdUser;
  }

  return Promise.reject(new Error('E-mail already exists'));
};

  const login = async (req) => {
    const { email, password } = req.body;
    let user;
    user = await User.findOne({
      where: { email: email }
    });

    if(!user) {
      return Promise.reject(new Error('User with this e-mail doesnt exists'));
    }

    if(await Utils.password.comparePassword(password, user.password)) {
      // console.log(email);
      // user = await User.update({
      //   token: Utils.token.getToken(email)
      // },
      // {where: { email: email }});
      return user;
    }

    return Promise.reject(new Error('Incorect password'));
};

module.exports = {
  reg,
  login
}